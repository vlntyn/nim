package kukushkin.nim.api;

import kukushkin.nim.ai.GameStrategy;
import kukushkin.nim.ai.SolveStrategyRandom;
import kukushkin.nim.model.GameModel;
import kukushkin.nim.model.MoveModel;
import kukushkin.nim.model.PlayerModel;
import kukushkin.nim.service.GameService;
import kukushkin.nim.service.MoveService;
import kukushkin.nim.service.PlayerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@RestController public class GameController {

    private final static Logger LOG = LoggerFactory.getLogger(GameController.class);
    private final static int MAX_NUM_STICKS = 13;

    @Autowired private GameService gameService;
    @Autowired private PlayerService playerService;
    @Autowired private MoveService moveService;



    /**
     * List all games with states
     * @return List of all games
     */
    @ResponseBody
    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(
            value = "/api/game",
            produces = {MediaType.APPLICATION_JSON_VALUE}
    ) public ResponseEntity<Collection<GameModel>> loadGameList() {

        final Collection<GameModel> games = gameService.findAll();
        return new ResponseEntity<>(games, HttpStatus.OK);
    }



    /**
     * Get game state by game id
     * @param id GameModel id
     * @return GameModel by its id
     */
    @ResponseBody
    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(
            value = "/api/game/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE
    ) public ResponseEntity<GameModel> loadGameById(@PathVariable("id") BigInteger id) {

        GameModel game = gameService.findById(id);

        if (game == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(game, HttpStatus.OK);
    }



    @ResponseBody
    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping(
            value = "/api/game",
            produces = MediaType.APPLICATION_JSON_VALUE
    ) public ResponseEntity<GameModel> createNewGame() {

        LOG.info("> creating new game");

        final PlayerModel firstPlayer = playerService.createNewPlayer("You");
        final PlayerModel secondPlayer = playerService.createNewPlayer("Computer");
        final GameModel game = gameService.createNewGame();

        game.setFirstPlayer(firstPlayer);
        game.setSecondPlayer(secondPlayer);
        game.setNextPlayer(game.getFirstPlayer());

        final Boolean[] array = new Boolean[MAX_NUM_STICKS];
        Arrays.fill(array, Boolean.TRUE);
        game.setSticks(array);
        game.setOver(false);

        return new ResponseEntity<>(game, HttpStatus.CREATED);
    }



    @ResponseBody
    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping(
            value = "/api/game/{id}/move",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    ) public ResponseEntity<GameModel> makeMove(@PathVariable("id") BigInteger id, @RequestBody MoveModel moveData) {

        final GameModel game = gameService.findById(id);

        //
        // user move
        // - - - - - - - - - - - - - - - - - - - - - - - - - -
        if (game == null) {
            /* game not found */
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        /* game exists */
        final MoveModel move = moveService.createMove();
        move.setGameId(game.getId());
        move.setPlayerId(game.getFirstPlayer().getId());


        Boolean[] sticks = game.getSticks();
        LOG.info("> currently available matchsticks: {}. {}", gameService.getAvailablePegsCount(sticks), Arrays.toString(sticks));
        LOG.info("> user selected matchsticks {} removed.", Arrays.toString(moveData.getMoves()));
        for (Integer removeIndex : moveData.getMoves()) {
            sticks[removeIndex] = false;
        }

        LOG.info("> available matchsticks after your move: {}. {}", gameService.getAvailablePegsCount(sticks), Arrays.toString(sticks));
        /* set game status after each move */
        game.setOver(gameService.gameOverCheck(game.getSticks()));
        /* computer is a second player in this game, give it the next move */
        game.setNextPlayer(game.getSecondPlayer());

        return new ResponseEntity<>(game, HttpStatus.OK);
    }



    @ResponseBody
    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(
            value = "/api/game/{id}/automove",
            produces = MediaType.APPLICATION_JSON_VALUE
    ) public ResponseEntity<GameModel> makeAutomatedMove(@PathVariable("id") BigInteger id) {

        final GameModel game = gameService.findById(id);

        Boolean[] sticks = game.getSticks();

        //
        // computer generated move to response to player's move
        // - - - - - - - - - - - - - - - - - - - - - - - - - -
        if (game.isOver()) {
            return new ResponseEntity<>(game, HttpStatus.OK);
        }

        final MoveModel autoMove = moveService.autoGenerateMove();
        autoMove.setGameId(game.getId());
        autoMove.setPlayerId(game.getSecondPlayer().getId());

        final GameStrategy gameStrategy = new GameStrategy();
        gameStrategy.setStrategy(new SolveStrategyRandom());

        /* get random number */
        int numPegsToBeRemoved = Math.min(gameStrategy.selectionCount(), gameService.getAvailablePegsCount(sticks));

        LOG.info(">> currently available matchsticks: {}. {}", gameService.getAvailablePegsCount(sticks), Arrays.toString(sticks));
        LOG.info(">> computer is going to remove {} matchstick(s).", numPegsToBeRemoved);

        final List<Integer> availablePegsIndexList = new ArrayList<>();
        for (int i = 0; i < game.getSticks().length; i++) {
            if (game.getSticks()[i] == true) {
                availablePegsIndexList.add(i);
            }
        }

        /* computer removes */
        for (int i = 0; i < numPegsToBeRemoved; i++) {
            game.getSticks()[availablePegsIndexList.get(i)] = false;
        }

        LOG.info("> available matchsticks after computer's move: {}. {}", gameService.getAvailablePegsCount(sticks), Arrays.toString(sticks));
        /* set game status after each move */
        game.setOver(gameService.gameOverCheck(game.getSticks()));
        game.setNextPlayer(game.getFirstPlayer());

        return new ResponseEntity<>(game, HttpStatus.OK);
    }
}
