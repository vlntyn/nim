package kukushkin.nim.service;

import kukushkin.nim.model.MoveModel;

import java.math.BigInteger;
import java.util.Collection;

public interface MoveService {

    MoveModel createMove();

    MoveModel autoGenerateMove();

    Collection<MoveModel> findAll();

    Collection<MoveModel> findAllByGameId(BigInteger gameId);
}
