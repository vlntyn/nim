package kukushkin.nim.service;

        import kukushkin.nim.api.GameController;
        import kukushkin.nim.model.PlayerModel;
        import org.slf4j.Logger;
        import org.slf4j.LoggerFactory;
        import org.springframework.stereotype.Service;

        import java.math.BigInteger;
        import java.util.HashMap;
        import java.util.Map;

@Service public class PlayerServiceBean implements PlayerService {

    Logger LOG = LoggerFactory.getLogger(PlayerServiceBean.class);

    private BigInteger nextId;
    private Map<BigInteger, PlayerModel> playerRepository;

    //

    @Override public PlayerModel createNewPlayer(final String name) {

        LOG.info("> create new player with name {}", name);
        PlayerModel player = new PlayerModel();
        player.setPlayerName(name);
        this.save(player);

        return player;
    }

    private PlayerModel save(final PlayerModel player) {

        if (playerRepository == null) {
            playerRepository = new HashMap<>();
            nextId = BigInteger.ONE;
        }

        player.setId(nextId);
        nextId = nextId.add(BigInteger.ONE);
        playerRepository.put(player.getId(), player);

        LOG.info("> player {} saved", player.getPlayerName());

        return player;
    }
}
