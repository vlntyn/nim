package kukushkin.nim.service;

import kukushkin.nim.model.GameModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Service public class GameServiceBean implements GameService {

    Logger LOG = LoggerFactory.getLogger(PlayerServiceBean.class);

//    @Autowired private GameRepository gameRepository;
//    @Autowired private PlayerRepository playerRepository;

    private BigInteger nextId;
    private Map<BigInteger, GameModel> gameRepository;

    //

    @Override public Collection<GameModel> findAll() {

        final Collection<GameModel> gameList;
        if (gameRepository == null) {
            gameRepository = new HashMap<>();
            nextId = BigInteger.ONE;
        }
        gameList = gameRepository.values();

        return gameList;
    }

    @Override public GameModel findById(BigInteger id) {

        if (gameRepository == null) {
            gameRepository = new HashMap<>();
            // todo:
            nextId = BigInteger.ONE;
        }

        final GameModel game = gameRepository.get(id);

        return game;
    }

    @Override public GameModel createNewGame() {

        final GameModel game = new GameModel();
        this.save(game);

        return game;
    }

    @Override public GameModel update(BigInteger id) {

        final GameModel gamePersisted = findById(id);
        if (gamePersisted == null) {
            // cannot update, game not found
            return null;
        }

//        GameModel updatedGame = gameRepository.save();
//        return updatedGame;
        return null;
    }

    @Override public void delete(BigInteger id) {
        GameModel game = gameRepository.get(id);
        if (game != null) {
            gameRepository.remove(game);
        }
    }

    @Override public boolean gameOverCheck(Boolean[] matchsticks) {
        for (Boolean matchstick : matchsticks) {
            if (matchstick) {
                return false;
            }
        }
        return true;
    }

    @Override public int getAvailablePegsCount(Boolean[] board) {
        int count = 0;
        for (boolean b : board) {
            if (b == true) {
                count++;
            }
        }
        return count;
    }

    private GameModel save(final GameModel game) {

        if (gameRepository == null) {
            gameRepository = new HashMap<>();
            nextId = BigInteger.ONE;
        }

        game.setId(nextId);
        nextId = nextId.add(BigInteger.ONE);
        gameRepository.put(game.getId(), game);

        LOG.info("> game {} saved", game.getId());

        return game;
    }
}
