package kukushkin.nim.service;

import kukushkin.nim.model.PlayerModel;

public interface PlayerService {

    PlayerModel createNewPlayer(String playerName);

}
