package kukushkin.nim.service;

import kukushkin.nim.model.GameModel;

import java.math.BigInteger;
import java.util.Collection;

/**
 * Define public business methods which we want to expose to the clients of this service.
 */
public interface GameService {

    Collection<GameModel> findAll();

    GameModel findById(BigInteger id);

    GameModel createNewGame();

    GameModel update(BigInteger id);

    void delete(BigInteger id);

    boolean gameOverCheck(Boolean[] matchsticks);

    int getAvailablePegsCount(Boolean[] board);
}
