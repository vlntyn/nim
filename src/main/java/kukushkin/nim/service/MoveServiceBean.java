package kukushkin.nim.service;

import kukushkin.nim.model.MoveModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Service public class MoveServiceBean implements MoveService {

    Logger LOG = LoggerFactory.getLogger(MoveServiceBean.class);

    //

    private BigInteger nextId;
    private Map<BigInteger, MoveModel> moveRepository;

    //

    @Override public MoveModel createMove() {
        LOG.info(">");
        LOG.info("> create new user move");
        MoveModel move = new MoveModel();
        this.save(move);

        return move;
    }

    @Override public MoveModel autoGenerateMove() {
        LOG.info(">>");
        LOG.info(">> create new computer move");

        // Simulate method execution time
        /*long pause = 5000;
        try {
            Thread.sleep(pause);
        } catch (Exception e) {
            // do nothing
        }
        LOG.info("Processing time was {} seconds.", pause / 1000);*/

        MoveModel move = new MoveModel();
        this.save(move);

        return move;
    }

    @Override public Collection<MoveModel> findAll() {

        final Collection<MoveModel> moves;
        if (moveRepository == null) {
            moveRepository = new HashMap<>();
            nextId = BigInteger.ONE;
        }
        moves = moveRepository.values();

        return moves;
    }

    @Override public Collection<MoveModel> findAllByGameId(BigInteger gameId) {

        Collection<MoveModel> filteredMoves = findAll()
                .stream()
                .filter(move -> move.getGameId().equals(gameId) )
                .collect(Collectors.toList());

        return filteredMoves;
    }

    //

    private MoveModel save(final MoveModel move) {
        if (moveRepository == null) {
            moveRepository = new HashMap<>();
            nextId = BigInteger.ONE;
        }

        move.setId(nextId);
        nextId = nextId.add(BigInteger.ONE);
        moveRepository.put(move.getId(), move);

        return move;

    }

}
