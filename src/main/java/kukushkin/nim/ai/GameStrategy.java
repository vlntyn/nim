package kukushkin.nim.ai;

public class GameStrategy {

    private SolveStrategy strategy;

    public void setStrategy(SolveStrategy strategy) {
        this.strategy = strategy;
    }

    public int selectionCount() {
        return strategy.action();
    }
}
