package kukushkin.nim.ai;

public class SolveStrategyRandom implements SolveStrategy {

    @Override public int action() {
        return (int)(Math.random() * 3) + 1;
    }

}
