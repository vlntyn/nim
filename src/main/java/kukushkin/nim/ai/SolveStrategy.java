package kukushkin.nim.ai;

public interface SolveStrategy {

    int action();

}
