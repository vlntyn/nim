package kukushkin.nim.enums;

public enum GameStrategyType {
    RANDOM_SELECTION,
    WIN_SELECTION
}
