package kukushkin.nim.enums;

public enum GameState {
    COMPUTER_WINS,
    PLAYER_WINS,
    GAME_OVER
}
