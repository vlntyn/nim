package kukushkin.nim;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication public class NimApplication {

    static final Logger LOG = LoggerFactory.getLogger(NimApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(NimApplication.class, args);

        LOG.info("\n" +
                ".__   __.  __  .___  ___. \n" +
                "|  \\ |  | |  | |   \\/   | \n" +
                "|   \\|  | |  | |  \\  /  | \n" +
                "|  . `  | |  | |  |\\/|  | \n" +
                "|  |\\   | |  | |  |  |  | \n" +
                "|__| \\__| |__| |__|  |__| \n" +
                "                          \n" +
                "\n" +
                "Welcome to Nim game!\n" +
                "Create a new game by sending a POST-XRH to 'http://localhost:8080/api/game'\n" +
                "e.g.: 'curl -i -X POST -H \"Content-Type:application/json\" -d \"{}\" http://localhost:8080/api/game',\n" +
                "or open your web browser on http://localhost:8080 for a visual experience.\n" +
                "And don't forget to read the ... manual (README.md).\n"

        );

//        LOG.info("Welcome to Nim game.");
//        LOG.info("Create a new game by sending a XRH to 'http://localhost:8080/api/game'");
//        LOG.info("e.g.: 'curl -i -X POST -H \"Content-Type:application/json\" -d \"{}\" http://localhost:8080/api/game'");
    }

}
