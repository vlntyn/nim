package kukushkin.nim.model;

import java.math.BigInteger;

public class MoveModel {

    private BigInteger id;

    private BigInteger gameId;

    private BigInteger playerId;

    private int[] moves;

    // Constructors

    public MoveModel() {} // JPA

    // Accessors


    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public BigInteger getGameId() {
        return gameId;
    }

    public void setGameId(BigInteger gameId) {
        this.gameId = gameId;
    }

    public BigInteger getPlayerId() {
        return playerId;
    }

    public void setPlayerId(BigInteger playerId) {
        this.playerId = playerId;
    }

    public int[] getMoves() {
        return moves;
    }

    public void setMoves(int[] moves) {
        this.moves = moves;
    }
}
