package kukushkin.nim.model;

import java.math.BigInteger;

public class PlayerModel {

    private BigInteger id;

    private String playerName;

    // Constructors

    public PlayerModel() {} // JPA

    // Accessors

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }
}
