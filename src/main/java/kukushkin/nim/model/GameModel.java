package kukushkin.nim.model;

import java.math.BigInteger;

public class GameModel {

    private BigInteger id;

    private PlayerModel firstPlayer;

    private PlayerModel secondPlayer;

    private PlayerModel nextPlayer;

    private Boolean[] sticks;

    private boolean isOver;

    // Constructors

    public GameModel() {} // JPA

    // Accessors

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public PlayerModel getFirstPlayer() {
        return firstPlayer;
    }

    public void setFirstPlayer(PlayerModel firstPlayer) {
        this.firstPlayer = firstPlayer;
    }

    public PlayerModel getSecondPlayer() {
        return secondPlayer;
    }

    public void setSecondPlayer(PlayerModel secondPlayer) {
        this.secondPlayer = secondPlayer;
    }

    public PlayerModel getNextPlayer() {
        return nextPlayer;
    }

    public void setNextPlayer(PlayerModel nextPlayer) {
        this.nextPlayer = nextPlayer;
    }

    public Boolean[] getSticks() {
        return sticks;
    }

    public void setSticks(Boolean[] sticks) {
        this.sticks = sticks;
    }

    public boolean isOver() {
        return isOver;
    }

    public void setOver(boolean over) {
        this.isOver = over;
    }
}
