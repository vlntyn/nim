package kukushkin.nim.ai;

import kukushkin.nim.AbstractTest;
import org.junit.Assert;
import org.junit.Test;

public class GameStrategyRandomTest extends AbstractTest {


    @Test public void testRandomNumbersAreInRange() {

        /* select strategy */
        final GameStrategy gameStrategy = new GameStrategy();
        gameStrategy.setStrategy(new SolveStrategyRandom());

        /* generate 100 values */
        int[] results = new int[100];
        for (int i = 0; i < results.length; i++) {
            results[i] = gameStrategy.selectionCount();
        }

        /* test each generated value is in bounds 1..3 */
        boolean testPassed = true;
        for (int i : results) {
            if (i < 1 || i > 3) {
                testPassed = false;
            }
        }

        Assert.assertTrue("Failure -- expected generated numbers are in range between 1..3", testPassed);
    }

}
