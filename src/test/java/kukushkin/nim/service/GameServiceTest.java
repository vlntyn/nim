package kukushkin.nim.service;

import kukushkin.nim.AbstractTest;
import kukushkin.nim.model.GameModel;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;

// @Transactional
public class GameServiceTest extends AbstractTest {

    @Autowired GameService gameService;

    @Before public void setUp() { }
    @After public void tearDown() { }

    @Test public void testFindAllWithNoGamesPresent() {
        final Collection<GameModel> list = gameService.findAll();
        Assert.assertNotNull("Failure -- expect not null", list);
        Assert.assertEquals("Failure -- expected size", 0, list.size());
    }
}
