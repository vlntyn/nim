package kukushkin.nim.api;

import kukushkin.nim.AbstractControllerTest;
import kukushkin.nim.model.GameModel;
import kukushkin.nim.model.MoveModel;
import kukushkin.nim.service.GameService;
import kukushkin.nim.service.MoveService;
import kukushkin.nim.service.PlayerService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigInteger;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class GameControllerTest extends AbstractControllerTest {

    @Autowired private GameService gameService;
    @Autowired private MoveService moveService;
    @Autowired private PlayerService playerService;

    @Before public void setUp() {
        super.setUp();
    }

    //

    // passes
    @Test public void testLoadGameList() throws Exception {

        String uri = "/api/game";

        MvcResult result = mvc
                .perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON))
                .andReturn();


        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();

        Assert.assertEquals("Failure -- expected HTTP status 200", 200, status);
        Assert.assertTrue("Failure -- expected HTTP response body to have a value", content.trim().length() > 0);

        System.out.println("> load game list" + content);
    }

    @Test public void testLoadGameByIdNotFound() throws Exception {

        String uri = "/api/game/";
        Long id = Long.MAX_VALUE;
        String finalUri = uri + id;

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.get(finalUri).accept(MediaType.APPLICATION_JSON_VALUE);

        MvcResult result = mvc
                .perform(request)
                .andExpect(status().isNotFound())
                .andReturn();

//        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();

        Assert.assertEquals("Failure -- expected HTTP status", 404, status);
//        Assert.assertTrue("Failure -- expected HTTP response body to be empty", content.trim().length() == 0);
    }

    @Test public void testLoadGameById() throws Exception {

        /* crate game and ignore its return object */
        String gameUri = "/api/game";
        mvc.perform(MockMvcRequestBuilders.post(gameUri).accept(MediaType.APPLICATION_JSON)).andReturn();

        /* find created game */
        String uri = "/api/game";
        BigInteger id = BigInteger.ONE;

        MvcResult result = mvc
                .perform(MockMvcRequestBuilders.get(uri, id).accept(MediaType.APPLICATION_JSON))
                .andReturn();

        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();

        Assert.assertEquals("Failure -- expected HTTP status 200", 200, status);
        Assert.assertTrue("Failure -- expected HTTP response body not to be empty", content.trim().length() > 0);

//        LOG.info("- - - -");
//        LOG.info(content);
//
//        List<GameModel> loadedGame = super.mapFromJson(content, List.class);
//
//        LOG.info(loadedGame.toString());
//
//        Assert.assertEquals("Failure -- received game with wrong id", id, loadedGame.get(0).getId());

    }

    @Test public void testCreateGame() throws Exception {

        String uri = "/api/game";
//        BigInteger id = BigInteger.ONE;

        MvcResult result = mvc
                .perform(MockMvcRequestBuilders.post(uri).accept(MediaType.APPLICATION_JSON))
                .andReturn();

        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();

        Assert.assertEquals("Failure -- expected HTTP status 201 (CREATED)", 201, status);
        Assert.assertTrue("Failure -- expected HTTP response body not to be empty", content.trim().length() > 0);

        GameModel createdGame = super.mapFromJson(content, GameModel.class);

        Assert.assertNotNull("Failure -- expected initialized game", createdGame);
        Assert.assertNotNull("Failure -- expected createdGame.id not null", createdGame.getId());
        Assert.assertNotNull("Failure -- expected createdGame.firstPlayer.id not null", createdGame.getFirstPlayer().getId());
        Assert.assertNotNull("Failure -- expected createdGame.secondPlayer.id not null", createdGame.getSecondPlayer().getId());

    }

    @Test public void testMakeMove() throws Exception {

        /* create new game */
        String gameUri = "/api/game";
        BigInteger gameId = BigInteger.ONE;

        mvc.perform(MockMvcRequestBuilders.post(gameUri, gameId).accept(MediaType.APPLICATION_JSON)).andReturn();

        /* create new move */
        String moveUri = "/api/game/{id}/move";
        BigInteger moveId = BigInteger.ONE;

        /* load created game */
        final GameModel game = gameService.findById(gameId);
        Assert.assertNotNull("Failure -- expected game not null", game);

        final MoveModel move = moveService.createMove();
        move.setGameId(game.getId());
        move.setPlayerId(game.getFirstPlayer().getId());
        int[] moves = {1, 2, 3};
        move.setMoves(moves);

        String requestBody = super.maptToJson(move);

        MvcResult result = mvc
                .perform(
                        MockMvcRequestBuilders
                                .post(moveUri, moveId)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                                .content(requestBody))
                .andReturn();

        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();

        Assert.assertEquals("Failure -- expected HTTP status 200", 200, status);
        Assert.assertTrue("Failure -- expected HTTP response body not to be empty", content.trim().length() > 0);

        GameModel createdGame = super.mapFromJson(content, GameModel.class);

        Assert.assertEquals("Failure -- expected game id to be 1", 1, createdGame.getId().intValue());
    }
}
