# Nim (Game)

Game server implementation.

## Tech stuff

- Java 8, Spring Boot 2, Maven 3, git

## Build & run

You can run the application using `mvn spring-boot:run`, or you can build an executable JAR file from source with `mvn clean package`.
Then you can run the JAR file: `java -jar target/nim-0.0.1-SNAPSHOT.jar`

The service should be up and running within a few seconds.

## REST API

`[POST] /game` -- create new game  
`[GET] /game` -- load list of all available games  
`[GET] /game/{:id}` -- load specified game  
`[PUT] /game/{:id}/move` -- make move in a game  
`[PUT] /game/{:id}/automove` -- ask opponent ("Computer") for a counter move

## Play the game

The game can be played over embedded web app at: [http://localhost:8080](http://localhost:8080) ([source code](https://gitlab.com/vlntyn/nim-angular)), or by making http-requests against the API.

### With the web app

Open your web browser and go to [`http://localhost:8080`](http://localhost:8080).
   
### With command line

1. First of all create a new game

    ```bash
    >_ curl -i -X POST -H "Content-Type:application/json" -d "{}" http://localhost:8080/api/game
    
   ```
   As a response you'll receive an object with initialized game
    ```json
    {
    "id":15,
    "firstPlayer":{"id":29,"playerName":"You"},
    "secondPlayer":{"id":30,"playerName":"Computer"},
    "nextPlayer":{"id":30, "playerName": "You"},
    "sticks":[true,true,true,true,true,true,true,true,true,true,true,true,true],
    "over":false
    }
    ```
    
    `id` -- is the game's id (obvious)  
    `firstPlayer` -- it is you (a human)  
    `secondPlayer` -- it is your nemesis (a computer)  
    `nextPlayer` -- indicates, who should make the next move  
    `sticks` -- initialized game field.  `true` means "Matchstick is still in game", `false` means the opposite and cannot be selected in a move.  
    `over` -- indicates whether the game is completed
    
2. Make your first move

    ```
    >_ curl -i -X PUT -H "Content-Type:application/json" -d "{"gameId": 15, "playerId": 29, "moves": [1, 3, 5]}" http://localhost:8080/api/game
    ```
    
    `moves` -- this property contains a list of indices of the `game.sticks` list.  
    Each element in that list must specify an index of the element (matchstick) that you want to be removed from `game.sticks`.
    You can only specify indices of available elements (`game.sticks[index] == true`).
    
3. Аsk the "Computer" player to make its move
   ```bash
    >_ curl -i -X GET -H "Content-Type:application/json" http://localhost:8080/api/automove
   ```
4. You play alternating with the "Computer" by repeating steps 2 and 3 until the values in the `game.sticks` list are all `false`.

5. The player who takes the last matchstick is loosing the game. Response object indicates `game.over: true` after the last turn. 